import gnome_reset_domain_set
import gobject
import gtk
import sys

dialog = gtk.FileChooserDialog ("Restore Preferences...", None,
                                gtk.FILE_CHOOSER_ACTION_OPEN,
                                (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                                 gtk.STOCK_OPEN, gtk.RESPONSE_OK))
dialog.set_default_response (gtk.RESPONSE_OK)
dialog.set_select_multiple (True)

def on_dialog_response (dialog, response):
    if response == gtk.RESPONSE_OK:
        for file in dialog.get_filenames ():
            gnome_reset_domain_set.restore(file)
    else:
        gtk.main_quit ()

dialog.connect ("response", on_dialog_response)
dialog.show ()

gtk.main ()
