import gnome_reset_domain_set
import gobject
import gtk
import gtk.glade
from gnome_reset_domain_directories import *

domain_set = gnome_reset_domain_set.DomainSet()

gladeui = gtk.glade.XML (PkgDataDir + "/gnome-reset.glade")
treeview = gladeui.get_widget ("treeview")
store = gtk.ListStore (gobject.TYPE_BOOLEAN, gobject.TYPE_STRING, gobject.TYPE_STRING);
treeview.set_model (store)

def on_fixed_toggled (cell, path_str, model):
    iter = model.get_iter_from_string (path_str)

    model.set (iter, 0, not model.get (iter, 0)[0])
    domain_set.set_active (model.get (iter, 2)[0], model.get (iter, 0)[0])

toggle_renderer = gtk.CellRendererToggle ()
toggle_renderer.connect ("toggled", on_fixed_toggled, store)
treeview.append_column (gtk.TreeViewColumn("Active", toggle_renderer, active=0))
treeview.append_column (gtk.TreeViewColumn("Description", gtk.CellRendererText(), text=1))

for domain in domain_set.domain_list():
    store.append ((domain.active, domain.description, domain.name))

def on_dialog_response (dialog, response):
    if response == 0 or response == 1:
        # Ask user for destination directory
        dialog = gtk.FileChooserDialog ("Select backup directory...", gladeui.get_widget ("dialog"),
                                        gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER,
                                        (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                                         gtk.STOCK_OPEN, gtk.RESPONSE_OK))
        dialog.set_default_response (gtk.RESPONSE_OK)

        response = dialog.run ()
        if (response == gtk.RESPONSE_CANCEL):
            dialog.destroy ()
            return

        # Perform backup
        domain_set.backup(dialog.get_filename ())
        if (response == 1):
            domain_set.reset()

        dialog.destroy ()
    else:
        gtk.main_quit()
    

gladeui.signal_autoconnect(globals())
gladeui.get_widget ("dialog").show_all()

gtk.main()
