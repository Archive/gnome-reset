class PreferenceDomain:
    def __init__(self, gconf_path):
        self.gconf_path = gconf_path;

    def backup (self):
        import os
        print "backup %s to %s: %s" % (self.gconf_path, self.location, self.name)
        os.system ("touch %s/%s" % (self.location, self.name))

    def restore (self, reset):
        if (reset):
            self.reset()
        print "restore %s from %s: %s" % (self.gconf_path, self.location, self.name)

    def reset (self):
        print "reset %s" % self.gconf_path
