import gconf

def createTextElement (document, name, text):
    node = document.createElement(name)
    textnode = document.createTextNode(text)
    superwritexml = node.writexml
    def writexml (writer, indent, addindent, newl):
        superwritexml (writer, "", "", "")
    node.writexml = writexml
    node.appendChild (textnode)
    return node

def getTypeName (type):
    if type == gconf.VALUE_INT:
        return "int"
    elif type == gconf.VALUE_FLOAT:
        return "float"
    elif type == gconf.VALUE_STRING:
        return "string"
    elif type == gconf.VALUE_BOOL:
        return "bool"
    return None

def createValue (document, value):
    child = None
    typename = getTypeName (value.type)
    if typename:
        child = createTextElement (document, typename, value.to_string())
        retval = document.createElement ("value")
        retval.appendChild (child)
        return retval
        
    if value.type == gconf.VALUE_LIST:
        child = document.createElement ("list")
        list_type = value.get_list_type();
        typename = getTypeName (list_type)
        if (typename):
            child.setAttribute ("type", typename)
        children = value.get_list()
        for c in children:
            childval = createValue (document, c)
            if childval:
                child.appendChild (childval)
        retval = document.createElement ("value")
        retval.appendChild (child)
        return retval
    elif value.type == gconf.VALUE_PAIR:
        child = document.createElement ("pair")
        car = document.createElement ("car")
        childval = createValue (document, value.get_car())
        if childval:
            car.appendChild (childval)
            child.appendChild (car)
        cdr = document.createElement ("cdr")
        childval = createValue (document, value.get_cdr())
        if childval:
            cdr.appendChild (childval)
            child.appendChild (cdr)
        retval = document.createElement ("value")
        retval.appendChild (child)
        return retval
    else:
        return None

def append_entry (document, xml, key, val, base):
    value = createValue (document, val)
    if value:
        child = document.createElement ("entry")
        if key[:len(base)] == base:
            key = key[len(base):]
        child.appendChild (createTextElement (document, "key", key))
        child.appendChild (value)
        xml.appendChild (child)

def append_entries (document, entrylist, baseentrylist, path, base):
    non_empty = 0
    value = client.get_without_default (path)
    if (value):
        append_entry (document, baseentrylist, path, value, "")
    entries = client.all_entries(path)
    for e in entries:
        if e.value and not e.get_is_default():
            append_entry (document, entrylist, e.key, e.value, base)
    subdirs = client.all_dirs(path)
    for s in subdirs:
        append_entries (document, entrylist, baseentrylist, s, base)


class PreferenceDomain:
    def __init__(self, gconf_paths):
        self.gconf_paths = map (lambda s: s.strip(), gconf_paths.split (","))
    def createDocument (self):
        import xml.dom
        impl = xml.dom.getDOMImplementation()
        document = impl.createDocument (None, None, None)

        entryfile = document.createElement ("gconfentryfile")
        document.appendChild (entryfile)

        baseentrylist = document.createElement ("entrylist")
        
        for path in self.gconf_paths:
            entrylist = document.createElement ("entrylist")
            entrylist.setAttribute ("base", path)

            base = path
            if base[-1] != "/":
                base += "/"

            append_entries (document, entrylist, baseentrylist, path, base)
            if entrylist.hasChildNodes():
                entryfile.appendChild (entrylist)

        if baseentrylist.hasChildNodes():
            entryfile.appendChild (baseentrylist)

        return document

    def filename (self):
        return self.location + "/" + self.name + ".entries"
        
    def backup (self):
        writer = file(self.filename(), "w")
        document = self.createDocument()
        document.writexml (writer, "", "\t", "\n")
        writer.close()

    def restore (self, reset):
        if (reset):
            self.reset()
        import os
        os.spawnlp (os.P_WAIT, "gconftool-2", "gconftool-2", "--load", self.filename())

    def reset (self):
        for path in self.gconf_paths:
            client.recursive_unset (path, 0)
        
client = gconf.client_get_default()
