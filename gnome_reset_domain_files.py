import os

def copy (fr, to):
    try:
        os.stat (fr)
        os.spawnlp (os.P_WAIT, "mkdir", "mkdir", "-p", os.path.dirname (to))
        os.spawnlp (os.P_WAIT, "cp", "cp", "-a", fr, to)
    except:
        pass

def delete (file):
    try:
        os.stat (file)
        os.spawnlp (os.P_WAIT, "rm", "rm", "-rf", file)
    except:
        pass

def cleanup (s):
    s = s.strip()
    if (s[-1] == '/'):
        s = s[:-1]
    return s

def real (s):
    return s.replace("%home%", (os.environ["HOME"]))

class PreferenceDomain:
    def __init__(self, file_paths):
        self.file_paths = map (cleanup, file_paths.split (","))

    def dirname (self):
        return self.location + "/" + self.name
        
    def backup (self):
        for path in self.file_paths:
            copy (real (path), self.dirname() + "/" + path)

    def restore (self, reset):
        for path in self.file_paths:
            copy (self.dirname() + "/" + path, real (path))

    def reset (self):
        for path in self.file_paths:
            delete (path.replace("%home%", os.environ["HOME"]))
