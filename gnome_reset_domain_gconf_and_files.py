import os
from gnome_reset_domain_gconf import PreferenceDomain as GConfDomain
from gnome_reset_domain_files import PreferenceDomain as FilesDomain

class PreferenceDomain:
    def __init__(self, gconf_paths, file_paths):
        self.gconfdomain = GConfDomain (gconf_paths)
        self.filesdomain = FilesDomain (file_paths)

    def backup (self):
        self.filesdomain.location = self.location
        self.filesdomain.name = self.name
        self.gconfdomain.location = self.location
        self.gconfdomain.name = self.name
        self.filesdomain.backup()
        self.gconfdomain.backup()

    def restore (self, reset):
        self.filesdomain.location = self.location
        self.filesdomain.name = self.name
        self.gconfdomain.location = self.location
        self.gconfdomain.name = self.name
        self.filesdomain.restore(reset)
        self.gconfdomain.restore(reset)

    def reset (self):
        self.gconfdomain.location = self.location
        self.gconfdomain.name = self.name
        self.filesdomain.location = self.location
        self.filesdomain.name = self.name
        self.gconfdomain.reset()
        self.filesdomain.reset()
