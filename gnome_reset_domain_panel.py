import os
from gnome_reset_domain_gconf_and_files import PreferenceDomain as GConfAndFilesDomain

class PreferenceDomain:
    def __init__(self):
        self.subdomain = GConfAndFilesDomain ("/apps/panel", "%home%/.gnome2/panel2.d")

    def backup (self):
        self.subdomain.location = self.location
        self.subdomain.name = self.name
        self.subdomain.backup()

    def restore (self, reset):
        self.subdomain.location = self.location
        self.subdomain.name = self.name
        self.subdomain.restore(reset)
        os.system ("killall gnome-panel")

    def reset (self):
        self.subdomain.location = self.location
        self.subdomain.name = self.name
        self.subdomain.reset()
        os.system ("killall gnome-panel")
