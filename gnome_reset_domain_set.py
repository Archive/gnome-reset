import os
import tempfile
import datetime
import xml.dom
import xml.dom.minidom
from gnome_reset_domain_directories import *


def spawn (directory, *args):
    if directory:
        pwd = os.getcwd()
        os.chdir(directory)
    os.spawnlp (os.P_WAIT, args[0], *args)
    if directory:
        os.chdir(pwd)

class DomainSet:
    def __init__ (self):
        self.domains = {}
        self.domain_dir_links = {}
        self.load_domains (PkgDataDir + "/xml")

    def loadPreferenceDomain (self, modname):
        mod = __import__ ("gnome_reset_domain_" + modname)
        return mod.PreferenceDomain

    def getText (self, node):
        text = ""
        for child in node.childNodes:
            if child.nodeType == child.TEXT_NODE:
                text += child.data
        return text
    
    def load_domains_file (self, path):
        document = xml.dom.minidom.parse(path)
        gnome_reset_domain_list = document.firstChild
        assert gnome_reset_domain_list.nodeName == "gnome-reset-domain-list"
        for gnome_reset_domain in gnome_reset_domain_list.childNodes:
            if gnome_reset_domain.nodeType != gnome_reset_domain.ELEMENT_NODE:
                continue
    #        try:
            assert gnome_reset_domain.nodeName == "gnome-reset-domain"
            module = None
            name = None
            parameters = None
            description = None
            for element in gnome_reset_domain.childNodes:
                if element.nodeType != element.ELEMENT_NODE:
                    continue
                if element.nodeName == "module":
                    assert module == None
                    module = self.getText (element)
                elif element.nodeName == "name":
                    assert name == None
                    name = self.getText (element)
                elif element.nodeName == "description":
                    assert description == None
                    description = self.getText (element)
                elif element.nodeName == "parameters":
                    assert parameters == None
                    parameters = {}
                    for parameter in element.childNodes:
                        if parameter.nodeType == parameter.ELEMENT_NODE:
                            parameters[str (parameter.nodeName)] = self.getText (parameter)
            assert module != None
            assert name != None
            assert description != None
            assert parameters != None
            self.domains[name] = self.loadPreferenceDomain (module)(**parameters)
            self.domains[name].name = name
            self.domains[name].description = description
            self.domains[name].active = False
    #        except:
    #            pass
            
    def load_domains_directory (self, directory):
        # Important that this come before the recurse below for avoiding loops
        self.domain_dir_links[directory] = directory
        for root, dirs, files in os.walk(directory):
            for path in dirs:
                if os.path.islink(path):
    #                try:
                        path = os.path.realpath(os.path.join (root, path))
                        # Avoid loops
                        if path not in self.domain_dir_links:
                            self.load_domains_directory (path)
    #                except:
    #                    pass
            for path in files:
                if path.endswith (".gnome-reset"):
                    self.load_domains_file (os.path.join (root, path))

    def domain_list (self):
        return self.domains.values();

    def load_domains (self, directory):
        self.load_domains_directory (os.path.realpath(directory))
    
    def backup (self, directory):
        domains_used = []
        tempdir = tempfile.mkdtemp ("", "gnome-reset-")
        tardir = os.path.join (tempdir, "gnome-reset-data")
        os.mkdir (tardir)
        for domain in self.domains.values():
            if domain.active:
                domain.location = tardir
                domain.backup()
                domains_used.append (domain.name + "\n")
        domain_list = file (os.path.join (tardir, "domain_list"), mode="w")
        domain_list.writelines (domains_used)
        domain_list.close()
        timestamp = datetime.datetime.now().strftime("%Y%m%dT%H%M%S")
        tarfile = tempfile.mkstemp (".tar.bz2", "gnome-reset-%s-" % (timestamp), directory)[1]
        spawn (tempdir, "tar", "jcf", tarfile, "gnome-reset-data")
        spawn (None, "rm", "-R", tempdir)

            
        

    def reset (self):
        for domain in self.domains.values():
    #        try:
                if domain.active:
                    domain.reset()
    #        except:
    #            pass

    def set_active (self, name, active = True):
        self.domains[name].active = active

def restore (tarfile, backup_first = True, reset = True):
    tarfile = os.path.realpath(tarfile)
    domain_set = DomainSet()
    tempdir = tempfile.mkdtemp ("", "gnome-reset-")
    tardir = os.path.join (tempdir, "gnome-reset-data")
    spawn (tempdir, "tar", "jxf", tarfile)
    domain_list = file (os.path.join (tardir, "domain_list"))
    domains_used = domain_list.readlines ()
    domain_list.close()
    domains_used = map (lambda x: x[:-1], domains_used)
    for domain_name in domains_used:
        domain_set.set_active(domain_name)
    if backup_first:
        domain_set.backup()
    for domain_name in domains_used:
        domain = domain_set.domains[domain_name]
        domain.location = tardir
        domain.restore(reset)
    spawn (None, "rm", "-R", tempdir)
